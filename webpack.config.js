/*
  eslint-disable
  import/unambiguous,
  import/no-commonjs,
  filenames/match-exported,
  filenames/match-regex
*/
const webpackDigest = require('@digest/webpack');

module.exports = webpackDigest;
